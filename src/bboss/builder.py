
# BBOSS Copyright (C) 2018, Laurent Marchelli
# SPDX-License-Identifier: GPL-3.0-or-later

import re
import shutil
from os import path
from io import StringIO
import fileinput

from jinja2 import BaseLoader, TemplateNotFound, Environment
from compose.config import environment
from compose.config import config
from compose.config import serialize
from bboss.node import NodeResolver
from bboss.nodereg import RegistryRender

_bboss_builders = {
    '.*':   'bboss_copyfile',
    '.jin': 'bboss_jinjavar',
    '.add': 'bboss_mergeall',
    }


def bboss_copyfile(target, source, env):
    """
    SCons builder used to copy source file content or the content of the file 
    symboly linked by the souce into the target file.
    Source file access permissions are also copied to the target.
    (https://www.scons.org/doc/production/HTML/scons-user.html#chap-builders-writing)

    target:
        List containing a single SCons Node object representing the target to
        be built by this builder function.
    source:
        List containing a single SCons Node objects representing the source to
        be used by this builder function to build the target.
    env:
        The SCons construction environment used to build the target. 
        An OverrideEnvironment which must contain a ["bboss_node"] variable.
        The Bboss node variable contains the target node in the Bboss project.
    """
    assert(len(target) == 1)
    assert(len(source) == 1)
    _ = env

    try:
        shutil.copy(str(source[0]), str(target[0]))
    except Exception:
        result = 1
    else:
        result = 0

    return result


def _substvar(src_path, trg_node):
    # Initialize local variables with Bboss target's node context
    resolver = NodeResolver()
    src_context = trg_node.context.inputs[0].context
    reg_project = src_context.project.registry
    reg_dict = {
        '${SRV_CURRENT}': ('SRV_%s', src_context.srv_current.registry),
        '${SRV_CONFIG}':  ('SRV_%s', src_context.srv_config.registry),
        '${PKG_CURRENT}': ('PKG_%s', src_context.srv_current.parent.registry),
    }

    # Compile the generic Regex for all possible Bboss variables, even 
    # contextual.
    file_out = StringIO()
    regex = re.compile(
    r"\$\{(BBOSS_)(?:(\$\{(?:SRV_CURRENT|SRV_CONFIG|PKG_CURRENT)\})((?:[A-Z]{2})?_))?([A-Z0-9_]+)\}")
    with open(src_path, 'r') as file_in:
        for line in file_in:
            for pre, ctx, typ, suf in regex.findall(line):
                var = pre + ctx + typ + suf
                val = None
                # Is it a global variable, non contextual ?
                if not ctx:
                    assert(not typ)
                    if reg_project:
                        val = resolver.find(reg_project, suf)
                # Is it a simple context ?
                elif typ == '_':
                    val = resolver.find(reg_dict[ctx][1], suf)
                # Context is used to compose another variable name
                else:
                    fmt, reg = reg_dict[ctx]
                    val = resolver.find(reg_project, 
                                        fmt % (reg.name + typ + suf))
                # Replace the variable by the value
                var = "${%s}" % var
                val = val.value if val is not None else ''
                line = line.replace(var, val)
            file_out.write(line)
    return file_out


class JinjaLoader(BaseLoader):


    def __init__(self, target_node):
        self.source_path = ''
        self.target_node = target_node


    def get_source(self, environment, template_path):
        _ = environment

        file_path = path.join(self.source_path, template_path)
        if not path.exists(file_path):
            raise TemplateNotFound(file_path)

        source = _substvar(file_path, self.target_node)
        self.source_path = path.dirname(file_path)

        return source.getvalue(), file_path, lambda: False


def bboss_jinjavar(target, source, env):
    """
    SCons builder used to replace variables inside the source file text by 
    their value and write the rendered jinja result into the target file.
    (https://www.scons.org/doc/production/HTML/scons-user.html#chap-builders-writing)

    target:
        List containing a single SCons Node object representing the target to
        be built by this builder function.
    source:
        List containing a single SCons Node objects representing the source to
        be used by this builder function to build the target.
    env:
        The SCons construction environment used to build the target. 
        An OverrideEnvironment which must contain a ["bboss_node"] variable.
        The Bboss node variable contains the target node in the Bboss project.
    """
    assert(len(target) == 1)
    assert(len(source) == 1)

    # Create the target stream with variables replaced by value
    # and let jinja render the target file.
    try:
        env = Environment(loader=JinjaLoader(env['bboss_node']))
        with open(str(target[0]), "w") as file_out:
            template = env.get_template(str(source[0]))
            file_out.write(template.render())
        # Copy access permissions from source to target 
        # (-x mode copy is required for shell scripts)
        shutil.copymode(str(source[0]), str(target[0]))
    except Exception:
        result = 1
    else:
        result = 0

    return result


def bboss_mergeall(target, source, env):
    """
    SCons builder used to merge several simple text files into one.
    (https://www.scons.org/doc/production/HTML/scons-user.html#chap-builders-writing)

    target:
        List containing a single SCons Node object representing the target to
        be built by this builder function.
    source:
        List containing a single SCons Node objects representing the source to
        be used by this builder function to build the target.
    env:
        The SCons construction environment used to build the target. 
    """
    assert(len(target) == 1)
    assert(len(source) >= 1)
    _ = env

    file_lst = [str(f) for f in source]
    try:
        with open(str(target[0]), "w") as file_out:
            lines = fileinput.input(file_lst)
            file_out.writelines(lines)
        # Copy access permissions from source to target 
        # (-x mode copy is required for shell scripts)
        shutil.copymode(str(source[0]), str(target[0]))
    except Exception:
        result = 1
    else:
        result = 0
    return result


def bboss_registry(target, source, env):
    """
    SCons builder used to complete the registry with informations stored into 
    service compose files (base image name and version).
    (https://www.scons.org/doc/production/HTML/scons-user.html#chap-builders-writing)

    target:
        List containing a single SCons Node object representing the target to
        be built by this builder function.
    source:
        List containing a single SCons Node objects representing the source to
        be used by this builder function to build the target.
    env:
        The SCons construction environment used to build the target. 
        An OverrideEnvironment which must contain a ["bboss_node"] variable.
        The Bboss node variable contains the target node in the Bboss project.
    """
    assert(len(target) == 1)
    assert(len(source) >= 1)

    # Initialize local variables with Bboss target's node context
    node_target = env['bboss_node']
    # Regex to split image description into base name and version
    regex = re.compile('([\w][\w.\-/]{0,127})[:]?(.*)?')
    result = 0
    try:
        for node_source in node_target.context.inputs:
            reg_node = node_source.context.srv_current.registry
            srv_name = reg_node.get_child('HOSTNAME').value
            # Read docker-compose.yml file
            cmp_file = config.ConfigFile.from_filename(str(node_source))
            srv_all  = cmp_file.config.get('services')
            if not srv_all:
                print("[ERROR] Service '%s' not found in %s from '%s'" % 
                      (srv_name, list(srv_all.keys()), str(node_source)))
                result = 1; continue
            # Try to get the requested services definition
            srv_def = srv_all.get(srv_name)
            if not srv_def:
                print("[ERROR] Service '%s' not found in %s from '%s'" % 
                      (srv_name, list(srv_all.keys()), str(node_source)))
                result = 1; continue
            # Try to get the image name
            srv_img = srv_def.get('image')
            if not srv_img:
                print("[ERROR] No image defined for service '%s' in '%s'." %
                      (srv_name, str(node_source)))
                result = 1; continue

            res = regex.match(srv_img)
            if not res:
                print("Unable to parse image name")
                result = 1; continue

            # Store the information into the registry
            img_from  = res.group(1)
            img_vers  = res.group(2)
            reg_img = reg_node.add('IMAGE')
            reg_img.add('FROM', img_from)
            reg_img.add('VERSION', img_vers if img_vers else 'latest')

        # If everything succeeded, dump the registry into the target file.
        if result == 0:
            with open(str(target[0]), "w") as file_out:
                file_out.write(RegistryRender(
                    node_target.context.project.registry).list)
    except Exception as e:
        _ = e
        result = 1

    return result


def bboss_dockconf(target, source, env):
    """
    SCons builder merging several docker-compose files into an intermediate one.
    Yaml syntax is checked, docker-compose syntax is none to allow partial 
    docker-compose file definition.

    target:
        A list containing a single SCons Node object representing the target 
        to be built by this builder function. ({package.name}-compose.yml)
    source:
        A list containing one or several SCons Node objects representing the
        source(s) to be used by this builder function to build the target.
        (service's docker-compose files of the target package)
    env:
        The SCons construction environment used to build the target. 
        An OverrideEnvironment which must contain a ["bboss_node"] variable.
        The bboss node variable contains the target node in the bboss project.
    """
    assert(len(target) == 1)
    assert(len(source) >= 1)
    _ = env

    # As we need to merge partial service definitions ,service validation 
    # function must be disabled by a temporary hook during the config.load.
    old_validate_service = config.validate_service
    def validate_service(*_): return
    config.validate_service = validate_service

    result = bboss_dockproj(target, source, env)
    config.validate_service = old_validate_service
    return result


def bboss_dockproj(target, source, env):
    """
    SCons builder merging several docker-compose files into an final one.
    Yaml and docker-compose syntaxes are checked.

    target:
        A list containing a single SCons Node object representing the target 
        to be built by this builder function. ({package.name}-compose.yml)
    source:
        A list containing one or several SCons Node objects representing the
        source(s) to be used by this builder function to build the target.
        (service's docker-compose files of the target package)
    env:
        The SCons construction environment used to build the target. 
        An OverrideEnvironment which must contain a ["bboss_node"] variable.
        The bboss node variable contains the target node in the bboss project.
    """
    assert(len(target) == 1)
    assert(len(source) >= 1)
    _ = env

    # Initialize local variables with Bboss target's node context
    base_dir = path.dirname(str(target[0]))
    try:
        compose_env     = environment.Environment.from_env_file(base_dir)
        compose_files   = [config.ConfigFile.from_filename(str(f)) 
                           for f in source]
        compose_details = config.ConfigDetails(base_dir, compose_files, 
                                               compose_env)
        compose_config  = config.load(compose_details)
        with open(str(target[0]), "w") as file_out:
            file_out.write(serialize.serialize_config(compose_config))

    except Exception as e:
        print("[ERROR] {}".format(e.msg))
        result = 1
    else:
        result = 0

    return result
