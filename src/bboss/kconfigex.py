
# BBOSS Copyright (C) 2018, Laurent Marchelli
# SPDX-License-Identifier: GPL-3.0-or-later

import io
import re
from os import path
from fnmatch import fnmatch

from kconfiglib import Kconfig, standard_config_filename, BOOL, STRING, INT, HEX

standard_kconfig_filename   = 'Kconfig.bboss'
standard_kconfig_filter     = 'Kconfig*.bboss'

class KconfigEx(Kconfig):
    """
    classdocs
    """

    def _open(self, filename, mode):
        old_file = super()._open(filename, mode)

        if mode != 'r':
            return old_file

        if not fnmatch(path.basename(filename), standard_kconfig_filter):
            return old_file

        # We are reading a Kconfig.bboss
        new_file = io.StringIO()
        var_regex = re.compile('\$\{([a-z_]*)\}')
        for line in old_file:
            # Replace all variables surrounded by braces by the corresponding
            # expended macro.
            for var in var_regex.findall(line):
                # Do not replace if the macro is not already defined, let Kconfig
                # handle the error.
                val = self.variables.get(var, None)
                if not val: continue

                # Replace the variable by the value in the line
                var = "${%s}" % var
                val = val.expanded_value if val.is_recursive else val.value
                line = line.replace(var, val)
            new_file.write(line)
        new_file.seek(0)

        return new_file

    def parse_symbol(self, symbol):
        assert(symbol.orig_type == STRING)

        regex   = re.compile(r"\$\{([A-Z0-9_]*)\}")
        symtext = symbol.str_value
        symdict = self.syms
        for var in regex.findall(symtext):
            sym = symdict.get(var, None)
            var = "${%s}" % var
            val = sym.str_value if sym else ''
            symtext = symtext.replace(var, val)
        return symtext

if __name__ == "__main__":
    from menuconfig import menuconfig
    kconfig = KconfigEx(standard_kconfig_filename)
    menuconfig(kconfig)
