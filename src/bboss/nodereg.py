
# BBOSS Copyright (C) 2018, Laurent Marchelli
# SPDX-License-Identifier: GPL-3.0-or-later

from anytree import RenderTree, PreOrderIter
from anytree.exporter.jsonexporter import JsonExporter

from bboss.node import NodeBase, NodeResolver

class RegistryNode(NodeBase):
    """
    A simple tree node with a `name` and any `kwargs`.
    See : https://anytree.readthedocs.io/en/latest/

    Overridden to create a Bboss variable Registry based on shell variable 
    separator. ('_')

    For instance, following variable list:
    BBOSS_BLD_SOURCE_CRE_REALPATH='./core':
    BBOSS_BLD_SOURCE_PKG_REALPATH='./package'
    BBOSS_BLD_SOURCE_EXP_REALPATH='./unstable'
    
    Will have following Registry structure:
    BBOSS
        BLD
            SOURCE
                CRE
                    RELPATH='./core'
                PKG
                    RELPATH='./package'
                EXP
                    RELPATH='./unstable'
    """
    separator = '_'

    # Protecting the value accessor: it is always accessible even not present
    # in the object. RegistryNode.value gives the default object value, until 
    # the object have its own 'value' in its internal dict (self.__dict__)
    value = ''

    def add(self, path, value = None):
        """
        Create a node and all its parent nodes if needed and add the value to
        the leaf node.

        path :
            Path of the branch to add (Registry's variable name).
        value :
            Value to set to the leaf (last node created or found).
        Returns :
            Return the last node created or found (leaf).
        """
        node = super().add(path)

        # Set the value to the leaf node if one is given
        if value is not None:
            # Temporary safety to avoid overwritting value.
            assert('value' not in node.__dict__)
            node.value = value

        return node

    def iter_value(self):
        """
        Iterate over this tree branch to only yield nodes with a value.
        """
        return PreOrderIter(self, filter_=lambda x: 'value' in x.__dict__)

    def save_json(self, filepath):
        with open(filepath, 'w') as file_out:
            JsonExporter(indent=2).write(self, file_out)

    def __getitem__(self, key):
        return NodeResolver().get(self, key).value

#     def __setitem__(self, key, value):
# 
#     def __delitem__(self, key):
# 
#     def __contains__(self, key):
# 
#     def __len__(self):

class RegistryRender(RenderTree):
    """
    Render Registry tree starting at given node.
    See : https://anytree.readthedocs.io/en/latest/

    Implemented to provide Registry smart output in tree shape as in variable 
    list or formated variable list shapes.
    """
    @property
    def list(self):
        """
        Display node's name equal value only for nodes holding a value.
        """
        return self.list_format()

    def list_format(self, fmt="%s=%r"):
        """
        Display node's and value in the given format only for nodes holding a
        value.
        """
        return  "\n".join(sorted([fmt % (node, node.value) 
                           for node in self.node.iter_value()]))

    def __str__(self):
        """
        Display node's name equal value if the node holds a value, otherwise
        just display the node's name.
        """
        return  "\n".join(
            ["%s%s%s" % (
                pre, node.name,
                "=%r" % node.value if 'value' in node.__dict__ else ''
                ) for pre, _, node in self])

