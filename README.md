# Build bundled open source softwares
[![PyPi](https://img.shields.io/pypi/v/bboss.svg)](https://pypi.python.org/pypi/bboss)
[![PyPI - Python Version](https://img.shields.io/pypi/pyversions/bboss)](https://pypi.python.org/pypi/bboss)
[![PyPI - License](https://img.shields.io/pypi/l/bboss)](https://pypi.python.org/pypi/bboss)
[![pipeline status](https://gitlab.com/bboss-org/bboss/badges/master/pipeline.svg)](https://gitlab.com/bboss-org/bboss/commits/master)
[![coverage report](https://gitlab.com/bboss-org/bboss/badges/master/coverage.svg)](https://gitlab.com/bboss-org/bboss/commits/master)
[![security: bandit](https://img.shields.io/badge/security-bandit-yellow.svg)](https://github.com/PyCQA/bandit)

<img src="https://gitlab.com/bboss-org/bboss/raw/master/docs/artwork/logo_black.png" width=200>

Bboss is a simple and efficient tool to build and run your network infrastructure on following hardware targets:

- Disk image (offline infrastucture, secured environment)
- Bare metal server
- Bare metal cluster (not available yet)

Bboss is too young to be used in production yet, but if you need to :

- Evaluate a product for your organization.
- Create a demonstration on continuous integration or continuous delivery.
- Mock-up network environment even on several separate networks.
- Check how to migrate your current product to another.

Bboss would be the right tool for you.<br>
I hope you will enjoy it.

## Distribution

Bboss brings its own distribution of open source softwares for :
  - Office Applications
  - Development
  - Administration

From the GUI, you can use the recommended configuration or select softwares you want in your project.<br>
Have look to the distribution content : [Bboss distribution](https://gitlab.com/bboss-org/bboss-distrib) 

## Bboss values

- A default configuration ready to go, start up and running.
- Simple configuration GUI, for everyone.
- Scripted configuration files to keep track of changes.
- Enhanced configuration for a better adaptation to your own environment.
- User customization separated from the official distribution to stay in-line with improvements.
- Self provisioning applications depending on your project configuration.

## System requirements

Bboss is designed to run on Linux and passed tests on following systems :<br>

|                                                           **Distribution**                                                            |          **Version**           |**Docker**|**Python**|**Git**|
|:-------------------------------------------------------------------------------------------------------------------------------------:|:------------------------------:|:--------:|:--------:|:-----:|
|<a href="https://centos.org/"><img height=50 src="https://upload.wikimedia.org/wikipedia/commons/9/9e/CentOS_Graphical_Symbol.svg"></a>|            CentOS 7            | 19.03.3  |  3.6.3   |2.16.5 |
|               <a href="https://www.debian.org/"><img height=50 src="https://www.debian.org/logos/openlogo-nd.svg"></a>                |       Debian 9 (stretch)       | 19.03.3  |  3.5.3   |2.11.0 |
|               <a href="https://www.debian.org/"><img height=50 src="https://www.debian.org/logos/openlogo-nd.svg"></a>                |       Debian 10 (buster)       | 19.03.3  |  3.7.3   |2.20.1 |
|        <a href="https://ubuntu.com/"><img height=50 src="https://gitlab.com/bboss-org/bboss/raw/master/docs/logos/ubuntu.svg"></a>        |Ubuntu 16.04 LTS (Xenial Xerus) | 19.03.3  |  3.5.2   | 2.7.4 |
|        <a href="https://ubuntu.com/"><img height=50 src="https://gitlab.com/bboss-org/bboss/raw/master/docs/logos/ubuntu.svg"></a>        |Ubuntu 18.04 LTS (Bionic Beaver)| 19.03.3  |  3.6.8   |2.17.1 |
|        <a href="https://ubuntu.com/"><img height=50 src="https://gitlab.com/bboss-org/bboss/raw/master/docs/logos/ubuntu.svg"></a>        |   Ubuntu 19.04 (Disco Dingo)   | 19.03.3  |  3.7.3   |2.20.1 |

**Hardware :**
- Processor : Intel core i5 or i7 (recommended)
- Ram : 16GB or 32GB (recommended)
- Hard disk space : 20GB or 30GB (recommended) 

**Software :**
- docker >= 19.03.2
- python >= 3.5.2
- git    >= 2.7.4
