# -*- mode: ruby -*-
# vi: set ft=ruby :

# Usually, host locale environment variables are passed to guest. 
# It may cause failures if the guest software do not support host locale. 
# One possible solution is override locale in the Vagrantfile:
# from : https://www.vagrantup.com/docs/vagrantfile/tips.html
# ENV["LC_ALL"] = "en_US.UTF-8"

distribs=[
  # CentOS 7
  { :name => 'centos7', :box => 'centos/7' },
  # Debian 9 (Stretch)
  { :name => 'stretch', :box => 'debian/stretch64' },
  # Debian 10 (Buster)
  { :name => 'buster',  :box => 'debian/buster64' },
  # Ubuntu 16.04 LTS (Xenial Xerus)
  { :name => 'xenial',  :box => 'ubuntu/xenial64' },
  # Ubuntu 18.04 LTS (Bionic Beaver)
  { :name => 'bionic',  :box => 'ubuntu/bionic64' },
  # Ubuntu 19.04 (Disco Dingo)
  { :name => 'disco',  :box => 'ubuntu/disco64' },
  # Ubuntu 19.10 (Eoan Ermine) Issues with virtual box guest addition
  { :name => 'eoan',  :box => 'ubuntu/eoan64' },
]

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://vagrantcloud.com/search.

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # NOTE: This will enable public access to the opened port
  # config.vm.network "forwarded_port", guest: 80, host: 8080

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine and only allow access
  # via 127.0.0.1 to disable public access
  # config.vm.network "forwarded_port", guest: 80, host: 8080, host_ip: "127.0.0.1"

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  # config.vm.network "private_network", ip: "192.168.33.10"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # config.vm.synced_folder "../data", "/vagrant_data"

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  config.vm.provider "virtualbox" do |vb|
    # Display the VirtualBox GUI when booting the machine
    vb.gui = false
    # Customize the amount of memory on the VM:
    vb.cpus = 2
    vb.memory = "2048"
    vb.customize ["modifyvm", :id, "--natdnsproxy1", "off"]
    vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
  end
  #
  # View the documentation for the provider you are using for more
  # information on available options.

  # Always use Vagrants insecure key
  config.ssh.insert_key = false

  # Enable provisioning with a shell script. Additional provisioners such as
  # Puppet, Chef, Ansible, Salt, and Docker are also available. Please see the
  # documentation for more information about their specific syntax and use.
  distribs.each do |dist|
    config.vm.define dist[:name], autostart: false do |node|
      node.vm.box = dist[:box]
      # Identify script url and path.
      script_url = "#{ENV['INSTALL_SCT_GETURL']}"
      script_url = '../../install.sh' if script_url.empty?
      script_path = "#{ENV['INSTALL_SCT_PRODUCT']}"
      script_path = script_path.empty? ? "${HOME}/install.sh" :
        "${HOME}/.#{script_path}/install.sh"
      # Identify script downloader.
      if not script_url.to_s.match('(http|https):\/\/') then
        node.vm.provision 'file', source: script_url, destination: script_path
        cmd_args = ['']
      elsif dist[:name] == 'centos7'
        cmd_args = ["curl -fsSL -o #{script_path} #{script_url}"]
      else
        cmd_args = ["wget -q -O #{script_path} #{script_url}"]
      end
      # Provisioning script
      env_prefix="^#{ENV['INSTALL_SCT_PRODUCT']}_".upcase
      node.vm.provision :shell,
        privileged: false,  # Do not execute shell script as sudo
        keep_color: true,   # Do not overwrite script colors
        env: {'NO_COLOR' => ENV['NO_COLOR']}.merge(
          ENV.select {|key, value| key =~ (/^INSTALL_SCT_/)}).merge(
          ENV.select {|key, value| key =~ (/#{env_prefix}/)}),
        args: cmd_args,
        inline: <<-SHELL
          mkdir -p ${HOME}/.bboss || exit ${?}
          # Downloading script
          if [[ -z "${1}" ]]; then
            echo -e "\nScript debug mode"
          else
            echo "${@}"; ${1} || exit ${?}
          fi
          bash "#{script_path}"
        SHELL
    end
  end
end
